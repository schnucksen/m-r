# m-r

Simple CQRS example from Greg Young ported partially to php to understand CRQS and EventSourcing.

## Installation

```txt
composer.phar install
```

## License

[The MIT License (MIT)][license]

