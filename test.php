<?php

require_once 'vendor/autoload.php';

$bus = new \SimpleCqrs\FakeBus();
$storage = new \SimpleCqrs\EventStore($bus);
$repository = new \SimpleCqrs\Repository($storage);
$bullshitDatabase = new \SimpleCqrs\BullShitDatabase();

$bus->registerHandler('SimpleCqrs\CreateInventoryItem', new \SimpleCqrs\CreateInventoryItemHandler($repository));
$bus->registerHandler('SimpleCqrs\RenameInventoryItem', new \SimpleCqrs\RenameInventoryItemHandler($repository));

$inventoryItemDetailView = new \SimpleCqrs\InventoryItemDetailView($bullshitDatabase);

$bus->registerHandler('SimpleCqrs\InventoryItemCreated', $inventoryItemDetailView);
$bus->registerHandler('SimpleCqrs\InventoryItemRenamed', $inventoryItemDetailView);

$id = uniqid();

$bus->send(new \SimpleCqrs\CreateInventoryItem($id, 'hans'));
$bus->send(new \SimpleCqrs\RenameInventoryItem($id, 'hans peter', 0));
$bus->send(new \SimpleCqrs\RenameInventoryItem($id, 'hans dieter', 1));

$readModel = new \SimpleCqrs\ReadModelFacade($bullshitDatabase);
$item = $readModel->getInventoryItemDetails($id);

print_r($item);