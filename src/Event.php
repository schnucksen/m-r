<?php

namespace SimpleCqrs;

class Event implements Message
{
    /**
     * @var int
     */
    public $version;
}