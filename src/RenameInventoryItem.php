<?php

namespace SimpleCqrs;

class RenameInventoryItem extends Command
{

    /**
     * @var string
     */
    public $inventoryItemId;

    /**
     * @var string
     */
    public $newName;

    /**
     * @var int
     */
    public $originalVersion;

    /**
     * @param $inventoryItemId
     * @param $newName
     * @param $originalVersion
     */
    public function __construct($inventoryItemId, $newName, $originalVersion)
    {
        $this->inventoryItemId = $inventoryItemId;
        $this->newName = $newName;
        $this->originalVersion = $originalVersion;
    }
}