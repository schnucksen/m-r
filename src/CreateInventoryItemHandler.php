<?php

namespace SimpleCqrs;

class CreateInventoryItemHandler
{
    /**
     * @var IRepository
     */
    private $repository;

    /**
     * @param IRepository $repository
     */
    public function __construct(IRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Command $message
     */
    public function handle(Command $message)
    {
        $item = new InventoryItem($message->inventoryItemId, $message->name);
        $this->repository->save($item, -1);
    }
}