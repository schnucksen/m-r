<?php

namespace SimpleCqrs;

interface ICommandSender
{
    /**
     * @param Command $command
     * @return void
     */
    public function send(Command $command);
}
