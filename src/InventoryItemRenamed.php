<?php

namespace SimpleCqrs;

class InventoryItemRenamed extends Event
{
    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $newName;

    /**
     * @param $id
     * @param $newName
     */
    public function __construct($id, $newName)
    {
        $this->id = $id;
        $this->newName = $newName;
    }
}