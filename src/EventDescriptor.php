<?php

namespace SimpleCqrs;

class EventDescriptor
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var int
     */
    public $version;

    /**
     * @var Event
     */
    public $event;

    /**
     * @param $id
     * @param Event $event
     * @param $version
     */
    public function __construct($id, Event $event, $version)
    {
        $this->id = $id;
        $this->event = $event;
        $this->version = $version;
    }
}