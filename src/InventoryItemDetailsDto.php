<?php

namespace SimpleCqrs;

class InventoryItemDetailsDto
{
    /**
     * @var id
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $currentCount;

    /**
     * @var int
     */
    public $version;

    /**
     * @param $id
     * @param $name
     * @param $currentCount
     * @param $version
     */
    public function __construct($id, $name, $currentCount, $version)
    {
        $this->id = $id;
        $this->name = $name;
        $this->currentCount = $currentCount;
        $this->version = $version;
    }
}