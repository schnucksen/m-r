<?php

namespace SimpleCqrs;

class FakeBus implements ICommandSender, IEventPublisher
{
    /**
     * @var array
     */
    private $handlers = [];

    /**
     * @param $id
     * @param $handler
     */
    public function registerHandler($id, $handler)
    {
        if (!isset($this->handlers[$id])) {
            $this->handlers[$id] = [];
        }

        $this->handlers[$id][] = $handler;
    }

    /**
     * @param Command $command
     */
    public function send(Command $command)
    {
        $commandName = get_class($command);

        if (!isset($this->handlers[$commandName])) {
            throw new \RuntimeException("No handler found for command {$commandName}");
        }

        if (count($this->handlers[$commandName]) > 1) {
            throw new \BadMethodCallException("Cannot send to more than 1 handler");
        }

        $this->handlers[$commandName][0]->handle($command);
    }

    /**
     * @param Event $event
     */
    public function publish(Event $event)
    {
        $eventName = get_class($event);

        if (!isset($this->handlers[$eventName])) {
            return;
        }

        foreach ($this->handlers[$eventName] as $handler) {
            $handler->handle($event);
        }
    }
}
