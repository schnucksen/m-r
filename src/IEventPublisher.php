<?php

namespace SimpleCqrs;

interface IEventPublisher
{
    /**
     * @param Event $event
     * @return void
     */
    public function publish(Event $event);
}