<?php

namespace SimpleCqrs;

interface IRepository
{
    /**
     * @param AggregateRoot $aggregate
     * @param $expectedVersion
     * @return mixed
     */
    public function save(AggregateRoot $aggregate, $expectedVersion);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
}