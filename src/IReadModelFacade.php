<?php

namespace SimpleCqrs;

interface IReadModelFacade
{
    /**
     * @return InventoryItemListDto
     */
    public function getInventoryItems();

    /**
     * @param  $id
     * @return InventoryItemDetailsDto
     */
    public function getInventoryItemDetails($id);
}