<?php

namespace SimpleCqrs;

class EventStore implements IEventStore
{
    /**
     * @var IEventPublisher
     */
    private $publisher;

    /**
     * @var array
     */
    private static $current = [];

    /**
     * @param IEventPublisher $publisher
     */
    public function __construct(IEventPublisher $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @param $aggregateId
     * @param $events
     * @param $expectedVersion
     * @throws ConcurrencyException
     */
    public function saveEvents($aggregateId, $events, $expectedVersion)
    {
        if (!isset(self::$current[$aggregateId])) {
            $current[$aggregateId] = [];
            $eventDescriptors = &self::$current[$aggregateId];
        } else {
            $eventDescriptors = &self::$current[$aggregateId];
            if ($eventDescriptors[count($eventDescriptors) - 1]->version != $expectedVersion && $expectedVersion != -1) {
                throw new ConcurrencyException();
            }
        }

        $i = $expectedVersion;

        // iterate through current aggregate events increasing version with each processed event
        foreach ($events as $event) {
            $i++;
            $event->version = $i;

            // push event to the event descriptors list for current aggregate
            $eventDescriptors[] = new EventDescriptor($aggregateId, $event, $i);

            // publish current event to the bus for further processing by subscribers
            $this->publisher->publish($event);
        }
    }

    /**
     * @param $aggregateId
     * @return array
     * @throws AggregateNotFoundException
     */
    public function getEventsForAggregate($aggregateId)
    {
        if (!isset(self::$current[$aggregateId])) {
            throw new AggregateNotFoundException();
        }

        $events = [];

        /** @var EventDescriptor $eventDescriptor */
        foreach(self::$current[$aggregateId] as $eventDescriptor) {
            $events[] = $eventDescriptor->event;
        }

        return $events;
    }
}