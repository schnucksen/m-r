<?php

namespace SimpleCqrs;

use Exception;

class AggregateNotFoundException extends Exception
{
}
