<?php

namespace SimpleCqrs;

abstract class AggregateRoot
{

    /**
     * @var array
     */
    protected $changes = [];

    /**
     * @var string
     */
    public $id;

    /**
     * @var int
     */
    public $version;

    /**
     * @return array
     */
    public function getUncommittedChanges()
    {
        return $this->changes;
    }

    public function markChangesAsCommitted()
    {
        $this->changes = [];
    }

    /**
     * @param Event[] $history
     */
    public function loadsFromHistory(array $history)
    {
        foreach ($history as $event) {
            $this->applyChange($event, false);
        }
    }

    /**
     * @param $event
     * @param bool $isNew
     */
    public function applyChange($event, $isNew = true)
    {
        $this->apply($event);

        if ($isNew) {
            $this->changes[] = $event;
        }
    }
}