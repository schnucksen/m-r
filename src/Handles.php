<?php

namespace SimpleCqrs;

interface Handles
{
    /**
     * @param $message
     * @return Command|Event
     */
    public function handle($message);
}