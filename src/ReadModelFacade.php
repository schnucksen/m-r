<?php

namespace SimpleCqrs;

class ReadModelFacade implements IReadModelFacade
{
    /**
     * @var BullShitDatabase
     */
    private $bullShitDatabase;

    /**
     * @param BullShitDatabase $bullShitDatabase
     */
    public function __construct(BullShitDatabase $bullShitDatabase)
    {
        $this->bullShitDatabase = $bullShitDatabase;
    }

    /**
     * @return InventoryItemListDto
     */
    public function getInventoryItems()
    {
        return $this->bullShitDatabase->list;
    }

    /**
     * @param  $id
     * @return InventoryItemDetailsDto
     */
    public function getInventoryItemDetails($id)
    {
        return $this->bullShitDatabase->details[$id];
    }
}