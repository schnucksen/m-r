<?php

namespace SimpleCqrs;

class InventoryItemDetailView implements Handles
{
    /**
     * @var BullShitDatabase
     */
    private $bullShitDatabase;

    /**
     * @param BullShitDatabase $bullShitDatabase
     */
    public function __construct(BullShitDatabase $bullShitDatabase)
    {
        $this->bullShitDatabase = $bullShitDatabase;
    }

    /**
     * @param Event $message
     */
    public function handle($message)
    {
        if ($message instanceof InventoryItemCreated) {
            $this->bullShitDatabase->details[$message->id] = new InventoryItemDetailsDto(
                $message->id, $message->name, 0, 0
            );
        } elseif ($message instanceof InventoryItemRenamed) {
            $inventoryItemDetailsDto = $this->getDetailsItem($message->id);
            $inventoryItemDetailsDto->name = $message->newName;
            $inventoryItemDetailsDto->version = $message->version;
        }
    }

    /**
     * @param $id
     * @return InventoryItemDetailsDto
     */
    protected function getDetailsItem($id)
    {
        if (!isset($this->bullShitDatabase->details[$id])) {
            throw new \RuntimeException("did not find the original inventory this shouldnt happen");
        }
        return $this->bullShitDatabase->details[$id];
    }
}