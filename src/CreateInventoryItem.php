<?php

namespace SimpleCqrs;

class CreateInventoryItem extends Command
{
    /**
     * @var mixed
     */
    public $inventoryItemId;

    /**
     * @var string
     */
    public $name;

    /**
     * @param $inventoryItemId
     * @param $name
     */
    public function __construct($inventoryItemId, $name)
    {
        $this->inventoryItemId = $inventoryItemId;
        $this->name = $name;
    }
}