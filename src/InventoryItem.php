<?php

namespace SimpleCqrs;

class InventoryItem extends AggregateRoot
{

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var bool
     */
    public $activated;

    /**
     * @param $id
     * @param $name
     */
    public function __construct($id = null, $name = null)
    {
        if ($id !== null && $name !== null) {
            $this->applyChange(new InventoryItemCreated($id, $name));
        }
    }

    /**
     * @param Event $event
     */
    public function apply(Event $event)
    {
        if ($event instanceof InventoryItemCreated) {
            $this->id = $event->id;
            $this->activated = true;
        } elseif ($event instanceof InventoryItemRenamed) {
            $this->name = $event->newName;
        }
    }

    /**
     * @param $newName
     */
    public function changeName($newName)
    {
        if (empty($newName)) {
            throw new \InvalidArgumentException('newName is empty.');
        }

        $this->applyChange(new InventoryItemRenamed($this->id, $newName));
    }
}