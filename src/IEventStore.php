<?php

namespace SimpleCqrs;

interface IEventStore
{
    /**
     * @param $aggregateId
     * @param $events
     * @param $expectedVersion
     * @return void
     */
    public function saveEvents($aggregateId, $events, $expectedVersion);

    /**
     * @param $aggregateId
     * @return Event[]
     */
    public function getEventsForAggregate($aggregateId);

}