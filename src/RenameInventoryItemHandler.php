<?php

namespace SimpleCqrs;

class RenameInventoryItemHandler
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Command $message
     */
    public function handle(Command $message)
    {
        /** @var InventoryItem $item */
        $item = $this->repository->getById($message->inventoryItemId);
        $item->changeName($message->newName);
        $this->repository->save($item, $message->originalVersion);
    }
}