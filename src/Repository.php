<?php

namespace SimpleCqrs;

class Repository implements IRepository
{
    /**
     * @var IEventStore
     */
    private $storage;

    /**
     * @param IEventStore $storage
     */
    public function __construct(IEventStore $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param AggregateRoot $aggregate
     * @param $expectedVersion
     * @return void
     */
    public function save(AggregateRoot $aggregate, $expectedVersion)
    {
        $this->storage->saveEvents($aggregate->id, $aggregate->getUncommittedChanges(), $expectedVersion);
    }

    /**
     * @param $id
     * @return InventoryItem
     */
    public function getById($id)
    {
        $events = $this->storage->getEventsForAggregate($id);

        $inventoryItem = new InventoryItem();
        $inventoryItem->loadsFromHistory($events);

        return $inventoryItem;
    }
}